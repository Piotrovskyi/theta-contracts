import dotenv from "dotenv";
dotenv.config();
import fetch from "node-fetch";
import fs from "fs";
import pathTool from "path";

const contract = process.argv[2];
console.log("downloading", contract);

async function main() {
  const opts = {
    module: "contract",
    action: "getsourcecode",
    address: contract,
    apikey: process.env.BSC_API_KEY,
  };
  const qs = Object.keys(opts)
    .map((key) => `${key}=${opts[key]}`)
    .join("&");
  const url = `https://api.bscscan.com/api?${qs}`;
  const res = await fetch(url).then((r) => r.json());
  const code = res.result[0].SourceCode;
  const folderName = `./.contracts/${res.result[0].ContractName}_${opts.address}`;
  console.log(code);
  if (!code.length) {
    console.log("not a contract?");
    return;
  }

  try {
    const sources = JSON.parse(code.slice(1, -1)).sources;
    const pathes = Object.keys(sources);
    for (let index = 0; index < pathes.length; index++) {
      const path = pathes[index];
      const fullPath = `${folderName}/${path}`;

      fs.mkdirSync(pathTool.dirname(fullPath), { recursive: true });
      fs.writeFileSync(fullPath, sources[path].content);
    }
  } catch (err) {
    console.log(err);
    const fullPath = `${folderName}/contract.sol`;
    fs.mkdirSync(pathTool.dirname(fullPath), { recursive: true });
    fs.writeFileSync(fullPath, code);
    console.log("success");
  }
}

main();
